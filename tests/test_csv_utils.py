import sys
from collections import Counter
from pathlib import Path
from unittest.mock import patch

import numpy as np
import pandas as pd
import pytest

sys.path.append(str(Path(__file__).parents[2].resolve()))
from fw_gear_precurate.csv_utils import (
    data2csv,
    keep_specified_keys,
    nested_get,
    sanitize_element,
)

test_data = [
    {
        "code": "sub-13 test",
        "firstname": None,
        "_id": "5db0845e69d4f3002d16ee05",
        "label": "sub-13 test",
        "lastname": None,
        "parents": {
            "group": "scien",
            "project": "5db0759469d4f3001f16e9c1",
            "session": {"session_no": 1, "session_info": None},
            "subject": None,
        },
        "permissions": {"perm-01": "nate-has-access"},
    },
    {
        "code": "sub-14",
        "firstname": None,
        "_id": "5db0845e69d4f3002d16ee05",
        "label": "sub-14",
        "lastname": "hello",
        "parents": {
            "group": "Nate",
            "project": "5db0759469d4f3001f16e9c1",
            "session": {"session_no": None, "session_info": None},
            "subject": None,
        },
        "permissions": {},
    },
]


def test_data2csv_runs():
    """data2csv cleans, renames, and returns new df"""
    test_df = pd.DataFrame.from_dict(
        {
            1: ["orig_acq_1", "33", "new_acq_1"],
            2: ["orig_acq_2", "44", "new_acq_2"],
            3: [np.nan, np.nan, np.nan],
            4: ["orig_acq_2", "44", "new_acq_2"],
        },
        columns=["Huey", "Louis", "Dewey"],
        orient="index",
    )
    _, new_df = data2csv(
        test_df,
        csv_label="maybe_today",
        prefix="or_not",
        column_rename=["Duck1", "Duck2", "Duck3"],
        user_columns=["new_acquisition_label", "num_expected_runs", "ignore"],
        save_csv=False,
    )
    # Test that nans AND duplicates were dropped
    assert new_df.shape == (2, 6)
    # Test that specific new columns were added
    compare = lambda x, y: Counter(x) == Counter(y)
    assert compare(
        [
            "Duck1",
            "Duck2",
            "Duck3",
            "new_acquisition_label",
            "num_expected_runs",
            "ignore",
        ],
        new_df.columns,
    )
    # Test that values within the new columns were initialized
    assert new_df["num_expected_runs"].values.tolist() == [1, 1]


@pytest.mark.parametrize(
    "mock_number, expected_type, expected",
    [
        (9, int, 9),
        (np.nan, float, np.nan),
        (1.3, float, 1.3),
    ],
)
@patch("fw_gear_precurate.csv_utils.sanitize_element")
def test_sanitize_element_numeric(mock_sanitize, mock_number, expected_type, expected):
    """Checks the handling of numeric inputs"""
    result = sanitize_element(mock_number)
    assert isinstance(result, expected_type)
    assert result is expected
    assert not mock_sanitize.called


@pytest.mark.parametrize(
    "mock_file, expected",
    [
        ("a_normal_file", "a_normal_file"),
        ("a file with spaces", "afilewithspaces"),
        ("55 - special?*3", "55-special_star3"),
    ],
)
def test_sanitize_element_str(mock_file, expected):
    """Checks the handling of str inputs"""
    result = sanitize_element(mock_file)
    assert result == expected


def test_nested_get():
    lvl1 = {"test": "data"}
    lvl2 = {"test": {"test": "data"}}
    lvl3 = {"test": {"test": {"test": "data"}}}

    dicts = [lvl1, lvl2, lvl3]
    keys = [["test"], ["test", "test"], ["test", "test", "test"]]
    result = "data"

    for dict, key in zip(dicts, keys):
        val = nested_get(dict, key)
        assert val == result
        print(f"expected: {result}, got: {val}, passed")


def test_keep_specified_keys():
    # This test may be obsolete now, but the method is useful and may need to be
    # re-integrated into the code. The test continues to pass, so keeping for now.
    keep_keys = [
        ["_id", "label", "lastname"],
        ["_id", ["parents", "group"], "label"],
        ["_id", ["permissions", "perm-01"], "label"],
        [
            ["parents", "subject"],
            ["parents", "session", "session_no"],
            ["parents", "session", "session_info"],
            "label",
        ],
    ]
    expected = [
        ["_id", "label", "lastname"],
        ["_id", "parents.group", "label"],
        ["_id", "permissions.perm-01", "label"],
        [
            "parents.subject",
            "parents.session.session_no",
            "parents.session.session_info",
            "label",
        ],
    ]

    for keys, exp in zip(keep_keys, expected):
        kept = keep_specified_keys(test_data, keys)
        for kept_dict in kept:
            print(kept_dict)
            print(exp)
            assert type(kept_dict) is dict
            assert set(kept_dict.keys()) == set(exp)
            if "parents.session.session_info" in kept_dict:
                assert kept_dict["parents.session.session_info"] == None
