import logging
import re
from unittest.mock import MagicMock

import pytest

from fw_gear_precurate.parser import (
    define_accepted_chars,
    get_hierarchy,
    validate_inputs,
)

from .conftest import GROUP_LABEL, PROJ_LABEL


def test_define_accepted_chars():
    """Does the method return an appended regex?"""
    result_pattern = define_accepted_chars(";?")
    assert result_pattern == re.compile("[^A-Za-z0-9;?]+")


@pytest.mark.parametrize(
    "mock_group, mock_project",
    [(None, None), (None, "some_project"), ("some_group", None)],
)
def test_get_hierarchy_needs_hierarchy(
    mock_group, mock_project, mock_destination, caplog
):
    """Does the method exit, if the hierarchy is missing?"""
    mock_destination.parents["group"] = mock_group
    mock_destination.parents["project"] = mock_project
    with pytest.raises(SystemExit):
        result_hierarchy, result_run_level = get_hierarchy(mock_destination)
    assert [
        rec.levelno == logging.ERROR
        for rec in caplog.records
        if "Unable to determine" in rec.getMessage()
    ]


def test_get_hierarchy_RuntimeError(mock_destination, caplog):
    """Does the gear error, if launched from the wrong level (not project or subject)?"""
    mock_destination.parent["type"] = "session"
    with pytest.raises(RuntimeError) as e_info:
        get_hierarchy(mock_destination)
        assert "Cannot run at session level" in e_info.value


def test_get_hierarchy_runs(mock_destination):
    """Can the gear find and establish the hierarchy from the gear context?"""
    result_hierarchy, result_run_level = get_hierarchy(mock_destination)
    assert result_hierarchy["group"] == GROUP_LABEL
    assert result_run_level == "project"


def test_validate_inputs_collects_tables(mock_context):
    """Does the gear return all tables, if inputs are provided?"""
    mock_context.get_input_path.return_value = "table"
    a, b, c = validate_inputs(mock_context)
    assert c


def test_validate_inputs_returns_empty_tuple_stage1(mock_context):
    """Does the gear return an empty tuple, if no inputs are provided?"""
    mock_context.get_input_path.return_value = None
    result = validate_inputs(mock_context)
    assert not result


def test_validate_inputs_exits_with_error(mock_context, caplog):
    """Does the gear error if some, but not all, table inputs are provided?"""

    def vary_side_effect(value):
        """Allow the definition of the side effect to be different, depending
        on the str arg passed to `get_input_path`"""
        if value in ["acquisition_table", "subject_table"]:
            return None
        elif value == "session_table":
            return "table"
        return value + 1

    mock_context.get_input_path = MagicMock(side_effect=vary_side_effect)

    with pytest.raises(SystemExit):
        result = validate_inputs(mock_context)
    assert [
        rec.levelno == logging.ERROR
        for rec in caplog.records
        if "inputs must be provided" in rec.getMessage()
    ]
