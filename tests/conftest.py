"""
Set up parameters for testing. Picked up by pytest automatically.
"""
import os.path as op
from pathlib import Path
from unittest.mock import MagicMock

import pandas as pd
import pytest
from flywheel_gear_toolkit.context.context import GearToolkitContext
from flywheel_gear_toolkit.testing.hierarchy import make_project

import tests.test_main_stage1

GROUP_LABEL = "sunshine"
PROJ_LABEL = "gobbletygook"
ASSETS_DIR = Path(Path(__file__).parent, "assets")


@pytest.fixture
def mock_project(n_subjs=3):
    proj = make_project(n_subs=n_subjs)
    return proj


@pytest.fixture
def mock_context(mocker, mock_project):
    mocker.patch("flywheel_gear_toolkit.GearToolkitContext")
    base_dir = op.dirname(op.abspath(tests.test_main_stage1.__file__))
    gtk = MagicMock(
        autospec=True,
        config={"debug": True},
        config_json={"inputs": {}},
        destination=mock_project.id,
        inputs={"api-key": "fake_key"},
        output_dir=op.join(base_dir, "test_output"),
    )
    return gtk


@pytest.fixture
def mock_destination():
    dest = make_project(
        par={
            "acquisition": None,
            "analysis": None,
            "group": GROUP_LABEL,
            "project": PROJ_LABEL,
            "session": None,
            "subject": None,
        }
    )
    dest.parent = {"id": dest.id, "type": "project"}
    return dest


@pytest.fixture
def mock_client(mocker):
    mocker.patch("flywheel_gear_toolkit.GearToolkitContext.client")
    client = MagicMock()
    return client


@pytest.fixture
def mock_edited_df():
    return pd.DataFrame(
        pd.read_csv(Path(ASSETS_DIR, "mock_acquisition_labels_example.csv"))
    )
