import logging
from unittest.mock import patch

import numpy as np
import pytest
from flywheel_gear_toolkit.testing.hierarchy import (
    make_acquisition,
    make_session,
    make_subject,
)

from fw_gear_precurate.main_stage1 import (
    collect_original_acqs,
    collect_original_sess,
    collect_original_subjs,
    search_acq_details,
)


def mock_acqs(n_acqs, mock_context, n_files=3, labels=["dummy_acq"]):
    """Populate img mock acquisition based off the mock context and capable of receiving
    new return values for testing.
    Note: this doesn't work as a fixture in conftest, so it lives here."""
    acq_list = []
    if n_acqs != len(labels):
        labels = n_acqs * labels[0]
    for n in range(n_acqs):
        acq = make_acquisition(labels[n], n_files=n_files)
        acq.files[n].modality = "colored_pencil"
        # acq.files[n].classification = {"Intent": "sunset", "Measurement": "big"}
        for i, img in enumerate(acq.files):
            img.type = "NIfTI"
            img.name = f"new_file_{str(i)}"
            img.info = {"SeriesNumber": i}
            # Arbitrary session number, used in excample_acq_returned.csv asset
            img.parents = {"session": 109}
            img.classification = {"Intent": "sunset", "Measurement": "big"}
        acq_list.append(acq)
    mock_context.acquisitions.iter_find.return_value = acq_list
    return mock_context, acq_list


def mock_sess(n_ses, mock_context):
    """Populate n_ses mock sessions based off the mock context for testing.
    Note: this doesn't work as a fixture in conftest, so it lives here."""
    sess_list = []
    for n in range(n_ses):
        ses = make_session(f"morning_{str(n)}")
        sess_list.append(ses)
    mock_context.sessions.iter_find.return_value = sess_list
    return mock_context, sess_list


def mock_subjs(n_subjs, mock_context):
    """Populate n_subjs mock subjects based off the mock context for testing.
    Note: this doesn't work as a fixture in conftest, so it lives here."""
    subj_list = []
    for n in range(n_subjs):
        subj = make_subject(f"afternoon_{str(n)}")
        subj_list.append(subj)
    mock_context.subjects.iter_find.return_value = subj_list
    return mock_context, subj_list


@patch(
    "fw_gear_precurate.main_stage1.data2csv", return_value=("fake_path", "pretend_df")
)
@patch(
    "fw_gear_precurate.main_stage1.search_acq_details",
    return_value=[["the world series", 2007, "was sad"]],
)
def test_collect_original_acqs_runs(
    mock_search, mock_d2csv, mock_context, mock_destination
):
    """Does the gear collect acqs and send them to collating method?"""
    n_acqs = 3
    mock_context, _ = mock_acqs(n_acqs, mock_context)
    result_df = collect_original_acqs(mock_context, mock_destination)
    assert mock_search.call_count == n_acqs
    assert mock_d2csv.called_once


@patch("fw_gear_precurate.main_stage1.data2csv")
def test_collect_original_sess_runs(mock_d2csv, mock_context, mock_destination):
    """Does the gear collect sessions or subjects and send them to collating method?"""
    n_mocks = 6
    mock_context, _ = mock_sess(n_mocks, mock_context)
    collect_original_sess(mock_context, mock_destination)
    # The iterator is only called once, even though the loop is len(n_ses)
    assert mock_context.sessions.iter_find.called_once
    assert mock_d2csv.called_once


@patch("fw_gear_precurate.main_stage1.data2csv")
def test_collect_original_subjs_runs(mock_d2csv, mock_context, mock_destination):
    """Does the gear collect sessions or subjects and send them to collating method?"""
    n_mocks = 4
    mock_context, _ = mock_subjs(n_mocks, mock_context)
    collect_original_subjs(mock_context, mock_destination)
    # The iterator is only called once, even though the loop is len(n_ses)
    assert mock_context.subjects.iter_find.called_once
    assert mock_d2csv.called_once


@patch("fw_gear_precurate.main_stage1.sanitize_name_general")
def test_search_acq_details_runs_nonfunc(mock_sanitize, mock_context):
    """Can the gear create a list of labels and series numbers for a given acq?"""
    n_files = 1
    mock_context, mock_acq_list = mock_acqs(
        1, mock_context, n_files=n_files, labels=["34 - isAgoodNumber"]
    )
    mock_sanitize.return_value = mock_acq_list[0].label
    result_list = search_acq_details(mock_acq_list[0])
    assert len(result_list) == n_files
    assert result_list == [
        ["isAgoodNumber", 0, 109],
    ]


@patch("fw_gear_precurate.main_stage1.sanitize_name_general")
def test_search_acq_details_runs_func(mock_sanitize, mock_context):
    """Can the gear create a list of labels and series numbers for a given acq?"""
    n_files = 6
    mock_context, mock_acq_list = mock_acqs(
        1, mock_context, n_files=n_files, labels=["34 - isAgoodNumber"]
    )

    for n in range(n_files):
        mock_acq_list[0].files[n]["classification"]["Intent"] = "Functional"
    mock_sanitize.return_value = mock_acq_list[0].label
    result_list = search_acq_details(mock_acq_list[0])
    assert len(result_list) == n_files
    assert result_list == [
        ["isAgoodNumber", 0, 109],
        ["isAgoodNumber", 1, 109],
        ["isAgoodNumber", 2, 109],
        ["isAgoodNumber", 3, 109],
        ["isAgoodNumber", 4, 109],
        ["isAgoodNumber", 5, 109],
    ]


@patch("fw_gear_precurate.main_stage1.sanitize_name_general")
def test_search_acq_details_uses_KeyError_nonfunc(mock_sanitize, mock_context):
    """Does the gear try to find the SeriesNumber, and if
    it doesn't find it, does it return a np.nan?
    """
    n_files = 1
    mock_context, mock_acq_list = mock_acqs(1, mock_context, n_files=n_files)
    mock_sanitize.return_value = mock_acq_list[0].label
    # Set up the code to enter KeyError condition
    for n in range(n_files):
        mock_acq_list[0].files[n].info.pop("SeriesNumber", None)
    result_list = search_acq_details(mock_acq_list[0])
    flat_list = [item for sublist in result_list for item in sublist]
    assert flat_list.count(np.nan) == n_files


@patch("fw_gear_precurate.main_stage1.sanitize_name_general")
def test_search_acq_details_uses_KeyError_func(mock_sanitize, mock_context):
    """Does the gear try to find the SeriesNumber, and if
    it doesn't find it, does it return a np.nan?
    """
    n_files = 6
    mock_context, mock_acq_list = mock_acqs(1, mock_context, n_files=n_files)
    for n in range(n_files):
        mock_acq_list[0].files[n]["classification"]["Intent"] = "Functional"

    mock_sanitize.return_value = mock_acq_list[0].label
    # Set up the code to enter KeyError condition
    for n in range(n_files):
        mock_acq_list[0].files[n].info.pop("SeriesNumber", None)
    result_list = search_acq_details(mock_acq_list[0])
    flat_list = [item for sublist in result_list for item in sublist]
    assert flat_list.count(np.nan) == n_files


@patch("fw_gear_precurate.main_stage1.sanitize_name_general")
def test_search_acq_details_warns_TypeError(mock_sanitize, mock_context, caplog):
    """Does the gear try to find the SeriesNumber, and if
    it doesn't find it, does it return a np.nan?
    """
    n_files = 1
    mock_context, mock_acq_list = mock_acqs(1, mock_context, n_files=n_files)
    mock_sanitize.return_value = mock_acq_list[0].label
    # Set up the code to enter KeyError condition
    for n in range(n_files):
        mock_acq_list[0].files[n].info.pop("SeriesNumber", None)
        mock_acq_list[0].files[n].classification = None
    result_list = search_acq_details(mock_acq_list[0])
    flat_list = [item for sublist in result_list for item in sublist]
    assert flat_list.count(np.nan) == n_files
    assert [rec.levelno == logging.WARNING for rec in caplog.records]


@patch("fw_gear_precurate.main_stage1.sanitize_name_general")
def test_search_acq_details_exit_for_dupes(mock_sanitize, mock_context, caplog):
    """Does the gear error out for duplicate acq labels (which should theoretically be impossible)?"""
    n_files = 4
    mock_context, mock_acq_list = mock_acqs(1, mock_context, n_files=n_files)
    mock_sanitize.return_value = mock_acq_list[0].label
    # Set up the code to enter KeyError condition
    for n in range(n_files):
        mock_acq_list[0].files[n].name = "cheddar"
        mock_acq_list[0].files[n]["classification"]["Intent"] = "Functional"
    result_list = search_acq_details(mock_acq_list[0])
    assert [
        rec.levelno == logging.CRITICAL
        for rec in caplog.records
        if "More than one nifti" in rec.getMessage()
    ]
    # Make sure the second-half of the method does not execute
    assert mock_sanitize.call_count == 0
