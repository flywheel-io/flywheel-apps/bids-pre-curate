import logging
import os
import sys
from collections import Counter
from pathlib import Path
from unittest.mock import MagicMock, patch

import flywheel
import numpy as np
import pandas as pd
import pytest

import fw_gear_precurate.main_stage2
from fw_gear_precurate.main_stage2 import (
    check_continuation_status,
    check_for_user_input_name,
    check_ignore_BIDS,
    check_run_counter,
    clean_orig_acqs,
    collect_uploaded_changes,
    match_entries,
    set_level_variables,
    update_acq_label_per_subj,
    update_labels,
    update_subject_labels,
    validate_df,
)
from tests.test_main_stage1 import mock_acqs, mock_sess, mock_subjs

ASSETS_DIR = Path(Path(__file__).parent, "assets")


def test_check_continuation_status_too_many(caplog):
    """Does the gear stop if there are too many actual scans relative to what is expected?"""
    mock_actual = {"one_fish": 2, "two_fish": 2}
    mock_expected = {"one_fish": 1, "two_fish": 2}
    with pytest.raises(SystemExit):
        check_continuation_status(mock_actual, mock_expected)

    assert [
        rec.levelno == logging.ERROR
        for rec in caplog.records
        if "Too many" in rec.getMessage()
    ]
    assert [
        rec.levelno == logging.CRITICAL
        for rec in caplog.records
        if "discrepancies" in rec.getMessage()
    ]


def test_check_continuation_status_not_enough(caplog):
    """Does the gear alert the user that there were missing scans relative to expected numbers?"""
    mock_actual = {"red_fish": 2, "blue_fish": 1}
    mock_expected = {"red_fish": 2, "blue_fish": 2}
    check_continuation_status(mock_actual, mock_expected)

    assert [
        rec.levelno == logging.INFO
        for rec in caplog.records
        if "Missing scan" in rec.getMessage()
    ]


@pytest.mark.parametrize(
    "mock_row, call_count, expected",
    [(["old_scan", "new_scan"], 1, "new_scan"), (["old_scan", ""], 0, "old_scan")],
)
@patch("fw_gear_precurate.main_stage2.sanitize_name_general")
def test_check_for_user_input_name_returns_label(
    mock_sanitize, mock_row, call_count, expected
):
    """Does the gear report back new labels, if supplied?"""
    mock_df = pd.DataFrame(
        np.array(mock_row).reshape(-1, len(mock_row)), columns=["existing", "new"]
    )
    result = check_for_user_input_name(mock_df.iloc[0], "existing", "new")
    assert mock_sanitize.call_count == call_count
    assert result == expected


@pytest.mark.parametrize(
    "mock_name, mock_ignore, expected_name",
    [
        ("Lewis", "z", "Lewis"),
        ("Chesterton", "T", "Chesterton_ignore-BIDS"),
        ("Mudderidge", "y", "Mudderidge_ignore-BIDS"),
        ("MacDonald", "F", "MacDonald"),
        ("Boreham", "x", "Boreham_ignore-BIDS"),
    ],
)
def test_check_ignore_BIDS(mock_name, mock_ignore, expected_name):
    """Does the gear appropriately append the _ignore-BIDS tag?"""
    mock_df = pd.DataFrame(
        np.array([mock_name, mock_ignore]).reshape(-1, 2),
        columns=["matters_not", "ignore"],
    )
    result = check_ignore_BIDS(mock_df.iloc[0], mock_name)
    assert result == expected_name


@pytest.mark.parametrize(
    "mock_label, expected_error",
    [("T1_mprage", "Mismatching specification"), ("T2w", "single value")],
)
def test_check_run_counter_errors(mock_label, expected_error, mock_edited_df, caplog):
    """Does the method catch user inconsistencies in the columns used to correct the project?"""
    check_run_counter(
        mock_edited_df.loc[mock_edited_df["existing_acquisition_label"] == mock_label]
    )
    assert [
        rec.levelno == logging.ERROR
        for rec in caplog.records
        if expected_error in rec.getMessage()
    ]


@pytest.mark.parametrize(
    "mock_label, expected_list",
    [("wm", ["func-bold_task-wm_run-01", "func-bold_task-wm_run-02"])],
)
def test_check_run_counter_runs(mock_label, expected_list, mock_edited_df):
    """If everything is specified consistently, does the method append run counter labels?"""
    result_list = check_run_counter(
        mock_edited_df.loc[mock_edited_df["existing_acquisition_label"] == mock_label]
    )
    compare = lambda x, y: Counter(x) == Counter(y)
    assert compare(result_list, expected_list)


def test_clean_orig_acqs(mock_edited_df):
    """Does the method reduce the input df to the correct columns and size?"""
    result_df = clean_orig_acqs(mock_edited_df)
    assert mock_edited_df.shape != result_df.shape
    assert result_df.shape == (mock_edited_df.shape[0], 2)
    assert all(
        col in ["existing_acquisition_label", "SeriesNumber"]
        for col in result_df.columns
    )
    assert not result_df.isnull().values.any()


def test_collect_uploaded_changes_errors(caplog):
    """Does the gear exit, if the num_expected_runs are not specified?"""
    mock_acq_df_loc = Path(ASSETS_DIR, "mock_acquisition_labels_bad_example.csv")
    with pytest.raises(SystemExit):
        collect_uploaded_changes([mock_acq_df_loc])
    assert [rec.levelno == logging.CRITICAL for rec in caplog.records]


def test_collect_uploaded_changes_runs(caplog):
    """Does the method return three dfs?"""
    mock_acq_df_loc = Path(ASSETS_DIR, "mock_acquisition_labels_example.csv")
    mock_ses_df_loc = Path(ASSETS_DIR, "mock_session_labels_example.csv")
    mock_subj_df_loc = Path(ASSETS_DIR, "mock_subject_codes_example.csv")
    a, b, c = collect_uploaded_changes(
        [mock_acq_df_loc, mock_ses_df_loc, mock_subj_df_loc]
    )
    assert not c.empty
    assert c.shape == pd.DataFrame(pd.read_csv(mock_ses_df_loc)).shape


def test_match_entries(mock_edited_df, caplog):
    """Does the gear match existing acq files with information about possible files supplied by the user?"""
    mock_acq_returned_df = pd.DataFrame(
        pd.read_csv(Path(ASSETS_DIR, "example_acq_returned.csv"))
    ).dropna(subset="new_acquisition_label", how="all")
    result_df = match_entries(mock_acq_returned_df, mock_edited_df)
    assert result_df.shape[0] == 7
    assert (
        len(
            [
                rec.levelno == logging.INFO
                for rec in caplog.records
                if "No specific" in rec.getMessage()
            ]
        )
        == 2
    )


@pytest.mark.parametrize(
    "mock_level, expected_find, expected_existing",
    [
        ("acquisitions", "parents.session=1234", "existing_acquisition_label"),
        ("sessions", "parents.project=1234", "existing_session_label"),
        ("projects", "parents.project=1234", "existing_project_label"),
    ],
)
def test_set_level_variables(mock_level, expected_find, expected_existing):
    """Does the method return the level-appropriate find str and column names?"""
    result_existing, _, result_find = set_level_variables(mock_level, "1234")
    assert result_find == expected_find
    assert result_existing == expected_existing


@patch("fw_gear_precurate.main_stage2.update_labels")
@patch("fw_gear_precurate.main_stage2.validate_df")
@patch("fw_gear_precurate.main_stage2.match_entries")
@patch("fw_gear_precurate.main_stage2.clean_orig_acqs")
@patch("fw_gear_precurate.main_stage2.collect_original_acqs")
def test_update_acq_label_per_subj_runs(
    mock_collect,
    mock_clean,
    mock_match,
    mock_validate,
    mock_update,
    mock_client,
    mock_project,
    mock_context,
    mock_edited_df,
):
    n_subjs = len(mock_project.subjects)
    mock_collect.return_value = mock_edited_df
    mock_match.return_value = mock_edited_df.iloc[0:5, :]
    mock_clean.return_value = pd.DataFrame(
        pd.read_csv(Path(ASSETS_DIR, "example_acq_returned.csv"))
    ).dropna(subset="new_acquisition_label", how="all")
    update_acq_label_per_subj(mock_edited_df, mock_client, mock_project)
    assert mock_collect.call_count == n_subjs
    assert mock_clean.call_count == n_subjs
    assert mock_match.call_count == n_subjs
    assert mock_validate.call_count == n_subjs
    assert mock_update.call_count == n_subjs


@patch(
    "fw_gear_precurate.main_stage2.check_ignore_BIDS",
    return_value="something_run-9823_more-stuff",
)
@patch("fw_gear_precurate.main_stage2.check_for_user_input_name")
def test_update_labels(
    mock_check_name, mock_check_bids, mock_edited_df, mock_context, mock_project, caplog
):
    """Does the method attempt to update the acquistion label?"""
    mock_project.acquisitions = MagicMock()
    labels = (
        mock_edited_df.loc[3:6, "existing_acquisition_label"]
        .dropna()
        .drop_duplicates()
        .values.tolist()
    )
    _, mock_project.acquisitions.iter_find.return_value = mock_acqs(
        1, mock_context, labels=labels
    )
    update_labels(mock_edited_df.iloc[3:6, :], mock_project, "acquisitions", "9876")
    assert mock_check_name.call_count == 3
    assert (
        len(
            [
                rec.levelno == logging.INFO
                for rec in caplog.records
                if "updating" in rec.getMessage()
            ]
        )
        == 3
    )


@pytest.mark.parametrize(
    "existing_subj_match, mock_existing_sess_label, log_msg",
    [
        (None, "", "updating new subject"),
        (True, None, "moving session"),
        (True, "morning_1", "would duplicate"),
    ],
)
@patch("fw_gear_precurate.main_stage2.delete_empty_subject")
@patch("fw_gear_precurate.main_stage2.sanitize_name_general")
def test_update_subject_labels(
    mock_sanitize,
    mock_delete,
    existing_subj_match,
    mock_existing_sess_label,
    log_msg,
    mock_project,
    mock_context,
    mock_client,
    caplog,
):
    # Fetch the pretend USER changes to the subject labels
    mock_subj_df = pd.DataFrame(
        pd.read_csv(Path(ASSETS_DIR, "mock_subject_codes_example.csv"))
    ).dropna(subset="new_subject_label", how="all")
    # Prep subjects for the mocked project
    mock_project.subjects = MagicMock()
    # Create subjects that can be substituted for the
    # placeholder "new_subj" object in the method.
    _, new_mock_subj_list = mock_subjs(1, mock_context)
    # Create pretend subjects to mimic unique subjects that will be
    # checked for naming updates
    _, existing_mock_subj_list = mock_subjs(1, mock_context)

    # Prep sessions for the mocked project
    mock_project.sessions = MagicMock(autospec=False)
    mock_context, mock_sess_list = mock_sess(2, mock_context)

    # Declare the placeholder "new_subj" substitution
    # "new_subj" = mock_client.get_subject
    mock_client.get_subject.return_value = new_mock_subj_list[0]

    if mock_existing_sess_label:
        # Push the method to the condition where the "new" label already exists
        mock_project.sessions.find_first.return_value = mock_sess_list[0]
        existing_mock_subj_list[0].sessions.iter = MagicMock(
            return_value=mock_sess_list
        )
    else:
        # Push the method to the condition where the "new" label is new
        mock_project.sessions.find_first.return_value = None
        existing_mock_subj_list[0].sessions.find_first = MagicMock(return_value=None)

    # In method, existing_subj = mock_project.subjects.find_first
    if existing_subj_match:
        # Give the mock a subject to run with
        mock_project.subjects.find_first.return_value = existing_mock_subj_list[0]
    else:
        # Force the method to the condition with no existing_subj
        mock_project.subjects.find_first.return_value = None

    update_subject_labels(mock_subj_df, mock_client, mock_project, "subjects")
    assert [
        rec.levelno == logging.INFO
        for rec in caplog.records
        if log_msg in rec.getMessage()
    ]


def test_validate_df_fails_runs_not_defined(caplog):
    """Does the gear complain if the user has not provided run number information?"""
    mock_subj_df = pd.DataFrame(
        pd.read_csv(Path(ASSETS_DIR, "mock_acquisition_labels_bad_example.csv"))
    ).dropna(subset="new_acquisition_label", how="all")
    with pytest.raises(SystemExit):
        validate_df(mock_subj_df)
    assert [
        rec.levelno == logging.CRITICAL
        for rec in caplog.records
        if "runs was not defined" in rec.getMessage()
    ]


def test_validate_df_fails_mismatch_runs(caplog):
    """Does the gear complain if the user has provided inconsistent run number information?"""
    mock_subj_df = pd.DataFrame(
        pd.read_csv(Path(ASSETS_DIR, "mock_acquisition_labels_example.csv"))
    ).dropna(subset="new_acquisition_label", how="all")
    with pytest.raises(SystemExit):
        validate_df(mock_subj_df)
    assert [
        rec.levelno == logging.CRITICAL
        for rec in caplog.records
        if "not consistent" in rec.getMessage()
    ]


@patch("fw_gear_precurate.main_stage2.check_run_counter")
@patch("fw_gear_precurate.main_stage2.check_continuation_status")
def test_validate_df_runs(mock_continuation, mock_counter):
    """Does the gear catch that there is one set of scans to add run counters to?"""
    mock_subj_df = pd.DataFrame(
        pd.read_csv(Path(ASSETS_DIR, "mock_acquisition_labels_example.csv"))
    ).dropna(subset="new_acquisition_label", how="all")
    mock_counter.return_value = mock_subj_df.loc[
        6:8, "new_acquisition_label"
    ].values.tolist()
    validate_df(mock_subj_df.iloc[:8, :])
    assert mock_continuation.called_once
    assert mock_counter.called_once
