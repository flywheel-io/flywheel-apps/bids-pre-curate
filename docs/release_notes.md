# Release notes

## 0.4.2:
### Bug fix
- nifti files are no longer counted within an acquisition UNLESS the intent is "Functional".
- acquisition labels are no longer sanitized when gathering original acquisition labels.  This used to lead to no matches being found if the label had spaces in it.

## 0.4.1
#### Bug fix
- main_stage1 should not include session IDs in the reported columns.

## 0.4.0
#### ENH
- Run counters have been added. The attempt is to automatically detect and label multiple runs. Always double-check the labeling is correct.
- The csv's to correct have new headers, which are used to match and change the labels.
- Acquisition labels no longer are matched verbatim.
  - SeriesNumbers are split into a secondary column, so that the SeriesDescription is the main matching criterion.
  - Regex is used (for the SeriesDescription) to find the potentially qualifying scans. Secondary checking against SeriesNumbers is used to hone results.

#### Docs
- Users: updated README helps define how to use the "num_expected_runs" column, introduced in this version.
- Devs: check the changelog for considerations around counters

#### Maintenance
- Code is significantly refactored from previous versions. Includes compatibility with subject-level changes in the future.
- Removed the integration testing with assets

## 0.3.1
#### Maintenance:
* Migrated to GitLab
* Added poetry
* Added skeleton items, CI, etc.

#### Enhancements:
* Changed the acquisition discovery agent to fw.acquisitions.iter_find to accomodate large studies and prevent timeouts.
