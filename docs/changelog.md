# Changelog

## 0.4.2
- Handling of multiple runs was restricted due to complications.  Further complications may still exist.
- Acquisition labels are no longer sanitized when gathering the original acquistion labels for the CSV

## 0.3.2  
Brianne: Handling multiple runs is now supported, but a little fragile.
- The acquisition label is now split apart to separate any prepending SeriesNumbers. This value is stored in the SeriesNumber column.
- The updating dataframes are sorted by SeriesNumber, so earlier series are presumed to be the earlier runs. (Assumption 1)
- The columns in the csv to correct are different. (SeriesNumber,new_acquisition_label,num_expected_runs,ignore) These values are used in the code and deviate from whatever had been planned with the previous modality, type, etc.

Decreased time:
- Acquisitions with no updates in the "new_acquisition_label" column are skipped
- Participant scans are checked for matching labels and SeriesNumber. When a participant is already fully compliant, no updating methods are entered.

## 0.2.0
Nate: Decided to add a changelog to keep more informal information about design decisions.  This is not required for every MR, but is strongly encourage when a big change or design change is made.  The more formal description of changes would live in the `docs/release_notes.md` doc.