
## Tasks

### General

- [ ] [Release Notes](https://gitlab.com/flywheel-io/flywheel-apps/templates/skeleton/-/blob/main/CONTRIBUTING.md#populating-release-notes)

- [ ] [Changelog](https://gitlab.com/flywheel-io/flywheel-apps/templates/skeleton/-/blob/main/CONTRIBUTING.md#adding-changelog-entry)

- [ ] Tests

### Gear specific (if applicable)

- [ ] Example run:

- [ ] Test data [added to data-registry](https://gitlab.com/-/ide/project/flywheel-io/qa/data-registry/edit/master/-/README.md#contributing-data)

- [ ] Regression test for gear added to Qase

