#!/usr/bin/env python3
"""Run the gear: set up for and call command-line command."""

import logging
import sys

import numpy as np
from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_precurate.main_stage1 import (
    collect_original_acqs,
    collect_original_sess,
    collect_original_subjs,
)
from fw_gear_precurate.main_stage2 import (
    collect_uploaded_changes,
    update_acq_label_per_subj,
    update_labels,
    update_subject_labels,
)
from fw_gear_precurate.parser import get_hierarchy, validate_inputs

log = logging.getLogger(__name__)


def main(gear_context):
    inputs = validate_inputs(gear_context)
    dest = gear_context.client.get_analysis(gear_context.destination["id"])
    hierarchy, run_level = get_hierarchy(dest)
    project = gear_context.client.get_project(hierarchy["project"])
    # Make sure all available fields are populated
    project = project.reload()

    if inputs:
        # Stage 2 - find and apply uploaded changes
        acq_df, subj_df, ses_df = collect_uploaded_changes(inputs)
        # Clear df's, if no updates.
        for df in [acq_df, subj_df, ses_df]:
            col_new_name = [col for col in df.columns if col.startswith("new_")][0]
            df.replace("", np.nan, inplace=True)
            df.dropna(subset=col_new_name, inplace=True)

        if not subj_df.empty:
            # Sub
            update_subject_labels(
                subj_df,
                gear_context.client,
                project,
                run_level,
                dry_run=gear_context.config["dry_run"],
            )
        if not ses_df.empty:
            # Ses
            update_labels(
                ses_df,
                project,
                "sessions",
                hierarchy["project"],
                dry_run=gear_context.config["dry_run"],
            )

        if not acq_df.empty:
            # Acq
            # If there are dupes, then organize run counters by SeriesNumber
            update_acq_label_per_subj(
                acq_df,
                gear_context.client,
                project,
                dry_run=gear_context.config["dry_run"],
            )

    else:
        # Stage 1
        log.info("Collecting the state of the current project (Stage 1)")
        # Methods are broken down by hierarchy level for future ability to run at given level
        collect_original_subjs(
            gear_context.client,
            project,
            gear_context.output_dir,
            gear_context.config["suggest"],
        )
        collect_original_sess(
            gear_context.client,
            project,
            gear_context.output_dir,
            gear_context.config["suggest"],
        )
        log.info(f"May take a minute to collect all acquisitions.")
        log.info("Building acquisitions CSV...")
        collect_original_acqs(
            gear_context.client,
            project,
            gear_context.output_dir,
            gear_context.config["suggest"],
        )

        log.info(
            "Please make changes to the csvs provided as output of this analysis. "
            "Re-upload the modified csvs to the project and set as inputs, when you "
            "re-run the gear (Stage 2)."
        )
    sys.exit(0)


if __name__ == "__main__":
    with GearToolkitContext() as gear_context:
        # Setup basic logging and log the configuration for this job
        gear_context.init_logging()
        main(gear_context)
