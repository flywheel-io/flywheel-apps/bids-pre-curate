import logging
import sys
from inspect import currentframe, getframeinfo
from math import isnan
from re import search

import flywheel
import numpy as np
import pandas as pd
from flywheel_gear_toolkit.utils.file import sanitize_name_general

from fw_gear_precurate.csv_utils import delete_empty_subject
from fw_gear_precurate.main_stage1 import collect_original_acqs

log = logging.getLogger(__name__)
# Ignore the SettingWithCopyWarning, b/c we want to change the type perpetually.
pd.options.mode.chained_assignment = None


def check_continuation_status(actual_counts: dict, expected_counts: dict):
    """
    Gear should exit if extra scans are present, since it can't guess which scans to keep.
    Args:
        actual_counts: records the acquisition label and number of occurrences
        expected_counts: records the study design specs for acquisition labels and occurrences.

    Returns:
        Exits, if too many scans.
        Reports, if not enough scans.

    """
    exit_status = 0
    diffs = set(expected_counts.items()) - set(actual_counts.items())
    if not diffs:
        diffs = set(expected_counts.items()) ^ set(actual_counts.items())
    for scan, count in diffs:
        if actual_counts[scan] > expected_counts[scan]:
            log.error(
                f"Too many scans available for {scan}. Check scanner console notes to determine if all scans should be included."
            )
            # Do not exit right away, but continue checking all other scans, to inform the user of
            # all of the discrepancies.
            exit_status += 1
        else:
            log.info(f"Missing scan(s) for {scan}. Check run labeling.")
    if exit_status > 0:
        log.critical("Correct the discrepancies before continuing.")
        sys.exit(1)


def check_for_user_input_name(
    row: pd.Series, df_col_existing_label: str, df_col_new_label: str
):
    """
    Check the user-given, corrected acquisition label for BIDS-compliance.
    """
    if row.get(df_col_new_label):
        if sanitize_name_general(row[df_col_new_label]) != row[df_col_new_label]:
            log.warning(f"New label f{row[df_col_new_label]} may not be BIDS compliant")
        return row[df_col_new_label]
    else:
        return row[df_col_existing_label]


def check_ignore_BIDS(row: pd.Series, new_name: str):
    """
    Attach label suffix to skip BIDS curation, if indicated in the user input csv.
    """
    true_conditions = ["t", "y", "x"]  # for "true", "yes", or "x"
    if row.get("ignore") and any(
        map(str(row["ignore"]).lower().startswith, true_conditions)
    ):
        new_name += "_ignore-BIDS"
    return new_name


def check_run_counter(rows: pd.DataFrame):
    """
    Try to handle run counters for a subject in an automated fashion.

    Args:
        rows (subset of pd.DataFrame): only rows from the subject's dataframe
                that have the matching existing acquisition label

    Returns:
        corrected_run_list: values to substitute back into the subject's dataframe in the new_acquisition_label column

    """
    num_expected_runs = rows["num_expected_runs"].drop_duplicates().to_list()
    run_entities = rows["new_acquisition_label"].to_list()
    corrected_run_list = []
    if len(num_expected_runs) > 1:
        log.error(
            f"Number of expected runs misspecified. Expecting a single value. "
            f"check_run_counter line {getframeinfo(currentframe()).lineno}"
        )
    elif len(run_entities) != int(num_expected_runs[0]):
        log.error(
            f"Mismatching specification for {list(set(run_entities))[0]} and number of expected runs: {num_expected_runs[0]}.\n"
            f"check_run_counter line {getframeinfo(currentframe()).lineno}"
        )
    else:
        for r, run in enumerate(run_entities):
            r += 1  # 1 indexed
            if "_run" in run:
                corrected_run_list.append(run)
            else:
                # Automatically zero pad
                corrected_run_list.append(run + f"_run-{r:02}")
    return corrected_run_list


def clean_orig_acqs(df: pd.DataFrame):
    """Remove extra columns and make sure that all the entries are typed as strs"""
    # get column labels not matching "existing_acquisition_label" or "SeriesNumber":
    drop_labels = df.columns[
        ~df.columns.isin(["existing_acquisition_label", "SeriesNumber", "session"])
    ]

    df = df.drop(columns=drop_labels)
    df = df.replace(np.nan, "")
    # The SeriesNumber MUST be recast as strings for initial match with the
    # corrected csv input.
    return df.applymap(str)


def collect_uploaded_changes(inputs: list):
    """Convert the user-corrected csvs to dataframes."""
    acq_df = pd.read_csv(inputs[0], dtype=str)
    if acq_df["num_expected_runs"].dropna().empty:
        log.critical(
            "Must define the expected number of runs for each acquisition label.\n"
            "If a run counter is defined, then the expected number of runs for that "
            "acquisition label is 1.\n"
            "If run counters are not defined in the new_acquisition_label (e.g., "
            "only a general sub-01_task-rest is repeated for multiple runs), "
            "then defined the expected number of runs."
        )
        sys.exit(1)
    ses_df = pd.read_csv(inputs[1], dtype=str)
    subj_df = pd.read_csv(inputs[2], dtype=str)
    for df in acq_df, ses_df, subj_df:
        df.fillna("", inplace=True)
    return acq_df, subj_df, ses_df


def match_entries(orig_acqs: pd.DataFrame, corrected_acq_df: pd.DataFrame):
    """Merge-style method to pare down the list of acquisitions in the corrected df.

    All the acquisitions from the orig_acqs df need to be present in the final df, but
    the information from the corrected_acq_df needs to populate the final df. The
    "merge" needs to be "left" for the list of acqs, but "right" for the information.
    Hence, this little method.

    Args:
        orig_acqs (pd.DataFrame): Subject-specific df with the 'existing_acquisition_label"
            and "SeriesNumber" populated. This df provides the "left merge" list of acquisitions
            to keep.
        corrected_acq_df (pd.DataFrame): Project-level, corrected csv with all headers, including
            "num_expected_runs".
    Returns:
        final_df (pd.DataFrame): df with only the acquisitions pertaining to the subject,
            but all necessary details to update the selected acquisitions.
    """
    final_df = pd.DataFrame(columns=corrected_acq_df.columns)
    # Clean the input dfs
    for df in [corrected_acq_df, orig_acqs]:
        df.replace("", np.nan, inplace=True)
    # Run through each entry for the files that the subject actually has (orig_acqs)
    for series_number, existing_acquisition_label in zip(
        orig_acqs["SeriesNumber"], orig_acqs["existing_acquisition_label"]
    ):
        # SeriesNumber is specified
        if not isnan(float(series_number)) and any(
            corrected_acq_df["existing_acquisition_label"].str.contains(
                existing_acquisition_label
            )
        ):
            # The import/export turns the SeriesNumber into a float. np.nan can't be converted.
            # So, drop the nan's and then compare the ints that are eventually converted from str.
            tmp = corrected_acq_df.dropna(subset="SeriesNumber")
            tmp["SeriesNumber"] = tmp["SeriesNumber"].astype(float, copy=False)
            try:
                matching_df = tmp.loc[
                    (tmp["existing_acquisition_label"] == existing_acquisition_label)
                    & (tmp["SeriesNumber"] == float(series_number))
                ]
                if matching_df.empty:
                    log.info(
                        f"No specific match for {series_number} {existing_acquisition_label}"
                    )
                else:
                    # Add only rows matching both acq label and SeriesNumber
                    final_df = pd.concat([final_df, matching_df])
            except:
                log.info(
                    f"No specific match for {series_number} {existing_acquisition_label}"
                )
        # SeriesNumber not specified
        elif any(
            corrected_acq_df["existing_acquisition_label"].str.contains(
                existing_acquisition_label
            )
        ):
            # Add all rows matching the acquisition label
            final_df = pd.concat(
                [
                    final_df,
                    corrected_acq_df.loc[
                        corrected_acq_df["existing_acquisition_label"]
                        == existing_acquisition_label
                    ],
                ],
            )
    # Fill in the session ID for validate_df matching
    final_df["session"] = list(set(orig_acqs["session"]))[0]
    return final_df[~final_df["new_acquisition_label"].isna()]


def set_level_variables(run_level: str, id_value: str):
    """Set variables based on the run-level, so that common methods can be used for processing."""
    run_level_singular = run_level[:-1]
    df_col_existing_label = "existing_" + run_level_singular + "_label"
    df_col_new_label = "new_" + run_level_singular + "_label"
    # id_value is either hierarchy['project'] or the subject.id field
    if run_level == "acquisitions":
        base_find = f"parents.session={id_value}"
    else:
        base_find = f"parents.project={id_value}"
    return df_col_existing_label, df_col_new_label, base_find


def update_acq_label_per_subj(
    corrected_acq_df: pd.DataFrame,
    fw: flywheel.client,
    project: flywheel.Project,
    dry_run: bool = False,
):
    """Use corrected values to update the acquisition labels for each subject."""

    subj_list = project.subjects.iter_find()
    for subj in subj_list:
        # Collect actual acqs that the participant has
        log.info(f"{subj.label}")
        orig_acqs = collect_original_acqs(fw, subj, stage1=False)

        if not orig_acqs.empty:
            # Drop extra cols and fill empty strings
            orig_acqs = clean_orig_acqs(orig_acqs)
            for s, sess in enumerate(set(orig_acqs["session"])):
                sess_orig_acqs = orig_acqs.loc[orig_acqs["session"] == sess]
                # Throw the list of existing acqs from orig_acqs against the main list
                # from corrected_acq_df and harvest the details in order to update.
                subj_acq_df = match_entries(sess_orig_acqs, corrected_acq_df)
                if subj_acq_df.empty:
                    log.info(f"Nothing to update for ses-{s+1:02}.")
                else:
                    # Check whether run entities are handled properly.
                    # Auto-generate run_counters, if appropriate.
                    subj_acq_df = validate_df(subj_acq_df.sort_values(["SeriesNumber"]))
                    # Now that the number of acquisitions have been checked, only search
                    # for the acquisition label to update once
                    subj_acq_df.drop_duplicates(
                        subset=["existing_acquisition_label", "SeriesNumber"],
                        inplace=True,
                    )
                    # Make the actual changes
                    update_labels(subj_acq_df, fw, "acquisitions", sess, dry_run)


def update_labels(
    df: pd.DataFrame,
    project: flywheel.Project,
    run_level: str,
    id_value: str,
    dry_run: bool = False,
):
    """
    Overarching method to change the acquisition labels based on user input and BIDS compliance.
    """
    log.info(f"Checking {run_level}")
    df_col_existing_label, df_col_new_label, base_find = set_level_variables(
        run_level, id_value
    )

    for index, row in df.iterrows():
        # Since len(rows) != len(project.acquisitions), need to find all acquisitions
        #   in the project for each row in the acquisition dataframe.
        # If names are numeric, the value has to be wrapped in double quotes
        try:
            # Check if numeric or hexadecimal
            int(row[df_col_existing_label])
            # find_string = f'{base_find},label="{row[df_col_existing_label]}"'
            find_string = f'{base_find},label="{row[df_col_existing_label]}"'
        except ValueError:
            # find_string = f"{base_find},label=~.?{row[df_col_existing_label]}$"
            find_string = f"{base_find},label=~.?{row[df_col_existing_label]}$"
        fw_iter_find = getattr(project, run_level)

        for i, fw_match in enumerate(fw_iter_find.iter_find(find_string)):
            new_name = check_for_user_input_name(
                row, df_col_existing_label, df_col_new_label
            )
            if row.get("ignore"):
                new_name = check_ignore_BIDS(row, new_name)
            run_level_singular = run_level[:-1]

            try:
                # Does fw_match have the same SeriesNumber? If so, update the acquisition
                # with the information from this row.
                sn = str(int(row["SeriesNumber"]))
                sn_check = sn in fw_match.label
            except (ValueError, KeyError):
                # In the case of empty SeriesNumber that matches, set to update
                sn_check = True

            run_entity = search(r".?run.?\d+_*", new_name)
            if run_entity:
                run_digits = search("\d+", run_entity.group(0))
                if run_digits.group(0) != f"{i+1:02}" and sn_check:
                    new_name = new_name.replace(run_digits.group(0), f"{i+1:02}")

            if new_name == row[df_col_existing_label] and sn_check:
                log.debug(
                    f"{run_level_singular} {row[df_col_existing_label]} did not change, not updating"
                )
            elif sn_check:
                level_label = getattr(fw_match, "label")
                if dry_run:
                    log.info(
                        f"Dry Run: NOT updating {run_level_singular} label from {level_label} to {new_name}"
                    )
                else:
                    log.info(
                        f"updating {run_level_singular} label from {level_label} to {new_name}"
                    )
                    level_update = getattr(fw_match, "update")
                    level_update({"label": new_name})
            else:
                # The SeriesNumber was entered, but did not match.
                # Do not update. (Also, would clutter log if a msg was included.)
                pass


def update_subject_labels(
    subj_df: pd.DataFrame,
    fw: flywheel.Client,
    project: flywheel.Project,
    run_level: str,
    dry_run: bool = False,
):
    """Rename subjects as specified in the corrected csv."""
    if run_level == "subject":
        log.info("Running at subject level, not adjusting subjects.")
        return
    subj_df.loc[subj_df["new_subject_label"] == "", "new_subject_label"] = subj_df[
        "existing_subject_label"
    ]
    # new_subject_label column should be all unique subjects
    unique_subjs = pd.unique(subj_df["new_subject_label"])
    for unique_subj in unique_subjs:
        if sanitize_name_general(unique_subj) != unique_subj:
            log.warning(f"Subject label {unique_subj} may not be BIDS compliant")
        existing_subj = project.subjects.find_first(f'label="{unique_subj}"')
        # Iterate over existing subjects that are supposed to have the same new_subject_label
        #   These are sessions that were misnamed and entered as subjects.
        #   All of these *subjects*  need to be converted to sessions under the new_subject_label subject
        subjects_to_be_moved = subj_df[subj_df["new_subject_label"] == unique_subj]
        for new_subject_label, existing_label_subject, subject_id in zip(
            subjects_to_be_moved["new_subject_label"],
            subjects_to_be_moved["existing_subject_label"],
            subjects_to_be_moved["id"],
        ):
            # If label is the same, we can skip.
            if new_subject_label == existing_label_subject:
                continue

            # If subject doesn't exist, update this subject to have the new label and code
            # otherwise, update sessions below it to point to this
            new_subj = fw.get_subject(subject_id)
            if existing_subj:
                for session in new_subj.sessions.iter():
                    existing_session = existing_subj.sessions.find_first(
                        f"label={session.label}"
                    )
                    # Check for a session with the same name in the destination
                    # this would cause a 409 error if we try to update the
                    # parent session, and lead to FLYW-8972
                    if existing_session:
                        log.warning(
                            f"Refusing to move session {session.label} as it "
                            f"would duplicate a session in {existing_subj.label}"
                        )
                        continue
                    # Change all sessions to point to the existing subject
                    to_update = {"subject": {"_id": existing_subj.id}}

                    if dry_run:
                        # Originally had '...to subject {existing_subj.label}' but this hasn't been updated yet
                        #   Locally, the two options are to cal fw.reload(), or use the name it was updated to
                        #   Since the latter is much more efficient, that's what I'll do.
                        log.info(
                            f"Dry Run: NOT moving session {session.label} to subject {unique_subj}"
                        )
                    else:
                        log.info(
                            f"moving session {session.label} to subject {unique_subj}"
                        )
                        session.update(to_update)

                # Once all sessions have been moved, delete this subject
                if delete_empty_subject(new_subj.id, dry_run, fw):
                    if dry_run:
                        log.info(f"Dry Run: NOT deleting Subject {new_subj.label}.")
                    else:
                        log.info(f"Subject {new_subj.label} deleted")
                else:
                    log.info(
                        f"Subject {new_subj.label} NOT deleted. "
                        f"Check to make sure it is empty. "
                        f"Continuing with next entry."
                    )
                    break
            else:
                if dry_run:
                    log.info(
                        f"Dry Run: NOT updating subject {new_subj.label} with new label, code of {unique_subj}"
                    )
                else:
                    log.info(
                        f"updating new subject {new_subj.label} with new label, code of {unique_subj}"
                    )
                    new_subj.update({"label": unique_subj, "code": unique_subj})
                # Update existing subject
                existing_subj = new_subj


def validate_df(subj_acq_df: pd.DataFrame):
    """
    Compare the subject acquisitions to the expected number of acquisitions as defined by the
    user in the acquisitions.csv

    The actual number of acquisitions and expected number are compared via dictionaries and
    the labels supplied in the "new_acquisition_label" column are sent to a secondary method to
    check for the run counter entity, when appropriate.
    Returns:
        subj_acqs_df (pd.DataFrame): corrected new_acquisition_label column with auto-generated
            run counters, when appropriate. No changes, when runs are uniquely identified.
    """
    unique_acq_labels = set(subj_acq_df["existing_acquisition_label"])
    expected_count_dict = {}
    for label in unique_acq_labels:
        # Technically could use validation that the user was consistent in providing
        # the expected number of runs. For now, assume the user input the number correctly
        runs = (
            subj_acq_df.loc[
                subj_acq_df["existing_acquisition_label"] == label, "num_expected_runs"
            ]
            .drop_duplicates()
            .values.tolist()
        )
        if len(runs) > 1:
            log.critical(
                f"Values for num_expected_runs is not consistent for {label}."
                f"Please correct the csv and reupload."
            )
            sys.exit(1)
        try:
            expected_count_dict.update({label: int(runs[0])})
        except ValueError:
            log.critical(
                "Number of expected runs was not defined. Please correct the csv, reupload, and try again."
            )
            sys.exit(1)
    # Most projects should have SeriesNumbers from the DICOM headers available for each scan
    # If all the SeriesNumbers are reported, use the SeriesNumber as the count value, since
    # SeriesNumber will be the most accurate quantification of the files that exist and should
    # match the expected_count_dict entries.
    if not any(subj_acq_df["SeriesNumber"].isna()):
        actual_count_dict = (
            subj_acq_df[["existing_acquisition_label", "SeriesNumber"]]
            .groupby(["existing_acquisition_label"])
            .count()
            .to_dict()["SeriesNumber"]
        )
    # If the SeriesNumbers are missing, fall back to another column that must have values.
    else:
        log.info("in the else")
        actual_count_dict = (
            subj_acq_df[["existing_acquisition_label", "num_expected_runs"]]
            .groupby(["existing_acquisition_label"])
            .count()
            .to_dict()["num_expected_runs"]
        )
        print(subj_acq_df)
        existing_label_df = subj_acq_df[
            ["existing_acquisition_label", "num_expected_runs"]
        ]
        print(existing_label_df)
        groups = existing_label_df.groupby(["existing_acquisition_label"])
        print(groups.describe())
        count = groups.count()
        print(count)

    print(actual_count_dict)
    print(expected_count_dict)
    if not expected_count_dict == actual_count_dict:
        check_continuation_status(actual_count_dict, expected_count_dict)

    for acq_label, num_of_runs in expected_count_dict.items():
        if num_of_runs > 1:
            # When there are multiple runs, use check_run_counter to rename the
            # entries to the "new_acquisition_label" column with incremented BIDS
            # run entities ("_run-0x").
            updated_runs = check_run_counter(
                subj_acq_df.loc[subj_acq_df["existing_acquisition_label"] == acq_label]
            )
            if updated_runs:
                subj_acq_df.loc[
                    subj_acq_df["existing_acquisition_label"] == acq_label,
                    "new_acquisition_label",
                ] = updated_runs
            else:
                log.error(
                    f"Could not update {acq_label}. Please check the preceding error."
                )
        continue
    return subj_acq_df
