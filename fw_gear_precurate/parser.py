import logging
import re
import sys

from flywheel_gear_toolkit import GearToolkitContext

log = logging.getLogger(__name__)


def define_accepted_chars(allowed="_-"):
    regex = r"[^A-Za-z0-9"
    hyphen_in = "-" in allowed
    for char in allowed:
        if hyphen_in and char == "-":
            continue
        regex += char
    regex += r"-]+" if hyphen_in else r"]+"
    try:
        safe_patt = re.compile(regex)
        return safe_patt
    except re.error:
        sys.exit(1)


def get_hierarchy(dest):
    """Determine from which level in the hierarchy the gear was launched."""
    run_level = dest.parent["type"]
    if run_level not in ["project", "subject"]:
        raise RuntimeError(
            f"Cannot run at {run_level} level, please run at"
            " subject or project level"
        )
    hierarchy = dest.parents
    if not (hierarchy["group"] and hierarchy["project"]):
        log.exception("Unable to determine run level and hierarchy, exiting")
        sys.exit(1)

    msg = "a single subject" if run_level == "subject" else "the whole project"
    log.info(f"Running on {msg}")
    return hierarchy, run_level


def validate_inputs(gear_context: GearToolkitContext):
    """
    Determine if the user is running the gear in stage 1 (make overview csvs) or stage 2 (use uploaded, corrected csvs).
    Returns:
        tuple of paths to acquisition, session, and subject corrected csvs
        OR
        empty tuple, indicating stage 1
        OR
        error, if the user entered partial information.
    """
    acq = gear_context.get_input_path("acquisition_table")
    ses = gear_context.get_input_path("session_table")
    subj = gear_context.get_input_path("subject_table")

    if acq and ses and subj:
        return (acq, ses, subj)
    elif not (acq or ses or subj):
        return ()
    else:
        log.error("No inputs or ALL inputs must be provided. Exiting")
        sys.exit(1)
