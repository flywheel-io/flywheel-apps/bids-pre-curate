import copy
import logging
import os
import os.path as op

import numpy as np
import pandas as pd
from flywheel_gear_toolkit.utils.file import sanitize_name_general

log = logging.getLogger(__name__)


def data2csv(
    data_df: pd.DataFrame,
    csv_label: str,
    prefix: str,
    column_rename: list = [],
    user_columns: list = [],
    suggest: bool = True,
    save_csv: bool = True,
    output_dir: os.PathLike = "/flywheel/v0",
    stage1: bool = True,
):
    """Creates a CSV on passed in data

    Create a CSV of passed in data while specifying which keys should be kept,
        how the columns should be named, what columns should be added, and whether
        or not to only use unique values

    Args:
        data_df (pd.DataFrame): contains the subset of items needed to rename
            the hierarchy objects.
        csv_label (str): project or subject label, used in the csv filename, if saved
        prefix (str): prefix under which to save file
        column_rename (list, optional): optional list of column titles in
            the same order as keep_keys to be displayed on the CSV.
        user_columns (list, optional): optional list of column titles
            to add to the csv
        suggest (boolean): whether or not to suggest new name by removing spaces
            and special characters
        save_csv (boolean): If false, the dataframe will be returned, but not saved.
        output_dir (os.Pathlike): directory to save the final csv

    Returns:
        tuple: csv_filename, dataframe

    """
    data_df.dropna(how="all", inplace=True)
    if column_rename:
        # Assuming the user can't modify this, we don't need to catch this error
        #  if len(column_rename) != len(data_df.columns):
        #      log.error(f'column_rename ({column_rename}) must be same length as the dataframe has ({data_df.columns}). Exiting')
        #      sys.exit(1)
        data_df.columns = column_rename

    if user_columns:
        data_df = data_df.reindex(columns=data_df.columns.tolist() + user_columns)
    if suggest:
        data_df.applymap(sanitize_element)
    data_df.sort_values([data_df.columns[0], data_df.columns[1]], inplace=True)

    if "num_expected_runs" in data_df.columns and stage1:
        # Set default in "num_expected_runs" column of a single run
        # Note: If "_run-0x" is specified in the "new_acquisition_label" column,
        # then 1 run is expected for that specific name.
        # Don't enter the total number of runs for that scan type.
        data_df["num_expected_runs"] = int(1)
    if stage1:
        data_df.drop_duplicates(inplace=True)

    csv_filename = op.join(output_dir, prefix + "_" + csv_label + ".csv")
    if save_csv:
        data_df.to_csv(csv_filename, index_label=False, index=False)

    return (csv_filename, data_df)


def sanitize_element(datum):
    """Send only strings to sanitize_name_general.
    Returns
        A properly formatted value to retain in the dataframe.
    """
    if isinstance(datum, (int, float)):
        return datum
    else:
        return sanitize_name_general(datum)


def keep_specified_keys(data, keep_keys):
    kept_data = []
    for datum in data:
        kept_data.append(
            {
                (key if type(key) is str else ".".join(key)): (
                    nested_get(datum, [key])
                    if type(key) is str
                    else nested_get(datum, key)
                )
                for key in keep_keys
            }
        )
    return kept_data


def nested_get(data_dict, keys):
    """Get values from a nested dictionary

    Args:
        data_dict:  Nested dictionary
            ex.
            {
                'key1': {
                    'key2': 'value'
                }
            }
        keys: List of keys to get.
            ex to get 'value' above, list of keys would be ['key1','key2']

    Returns:
        Value at nested key.

    """
    data_copy = copy.copy(data_dict)
    for k in keys:
        if k not in data_copy:
            log.warning(f"key {k} not in dictionary, setting to default value of None.")

        data_copy = data_copy.get(k)
    return data_copy


def delete_empty_subject(subject_id, dry_run, fw):
    """Deletes a subject after confirming its empty.

    A subject will be empty if it has no sessions attached to it, and
    if it has no files attached to it. This function checks whether
    the given subject has any children sessions or files.  If it
    doesn't, it deletes the subject and returns true, if it does,
     it returns false and logs a warning

    Args:
        subject_id (str): the subject id

    Returns:
       boolean: whether the subject was deleted or not

    """
    subject = fw.get_subject(subject_id)
    sessions = subject.sessions
    if dry_run:
        return True

    if len(sessions.find()):
        log.warning("Subject not empty: sessions still attached. Not deleting")
        return False
    elif len(subject.files) > 0:
        log.warning("Subject not empty: files still attached.  Not deleting")
        return False
    else:
        fw.delete_subject(subject.id)
        return True
