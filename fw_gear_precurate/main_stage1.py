import logging
import re
import sys
from pathlib import Path

import flywheel
import numpy as np
import pandas as pd
from flywheel_gear_toolkit.utils.file import sanitize_name_general

from fw_gear_precurate.csv_utils import data2csv

log = logging.getLogger(__name__)


def collect_original_acqs(
    fw: flywheel.Client,
    hierarchy_obj: [flywheel.Project, flywheel.Subject],
    output_dir: Path = "/flywheel/v0/output",
    suggest: bool = True,
    stage1: bool = True,
):
    """
    Search the project to find all the acquisitions with their various SeriesNumbers.
    Add more columns to user_columns, if greater specificity is desired in the future.
    Returns:
        Automatically populates csv report of all acquisitions and available SeriesNumbers,
        run from the project level.
        orig_acqs_df (pd.DataFrame): formatted dataframe that is ready for users to clean
    """
    acq_rows = []
    for acq in fw.acquisitions.iter_find(
        f"parents.{hierarchy_obj.container_type}={hierarchy_obj.id}"
    ):
        print(acq.label)
        new_rows = search_acq_details(acq, False)
        if new_rows:
            acq_rows.extend(new_rows)

    orig_acqs_df = pd.DataFrame()
    if acq_rows:
        acq_df = pd.DataFrame(acq_rows)
        # Add session info if stage2 for sorting and run counters
        if stage1:
            # Session IDs are not needed for Stage 1 and, in fact,
            # would interfere with de-duplication.
            acq_df = acq_df.drop(columns=acq_df.columns[-1], axis=1)
            cols = ["existing_acquisition_label", "SeriesNumber"]
        else:
            # Keep session IDs for stage 2 to enable run counter functionality
            cols = ["existing_acquisition_label", "SeriesNumber", "session"]

        try:
            acq_df.columns = cols
        except ValueError:
            # In the event that imported data do not have series numbers
            acq_df.columns = cols.remove("SeriesNumber")

        if stage1:
            # Stage 1 needs unique entries
            acq_df = acq_df.drop_duplicates()
        else:
            # Retain duplicates, if stage 2, so that run counters can work correctly.
            pass

        if not acq_df.empty:
            (_, orig_acqs_df) = data2csv(
                acq_df,
                hierarchy_obj.label,
                prefix="acquisition_labels",
                column_rename=cols,
                user_columns=["new_acquisition_label", "num_expected_runs", "ignore"],
                suggest=suggest,
                save_csv=stage1,
                output_dir=output_dir,
                stage1=stage1,
            )
    return orig_acqs_df


def collect_original_sess(
    fw: flywheel.Client,
    project: flywheel.Project,
    output_dir: Path = "/flywheel/v0/output",
    suggest: bool = True,
):
    """
    Search the project to find the commonly named sessions.

    Returns:
        csv report of all the commonly named sessions.
    """
    ses_rows = []
    for ses in fw.sessions.iter_find(f"parents.project={project.id}"):
        ses_rows.append(ses.label)
    ses_df = pd.DataFrame(ses_rows).drop_duplicates()
    log.info("Building session CSV...")
    if not ses_df.empty:
        data2csv(
            ses_df,
            project.label,
            prefix="session_labels",
            column_rename=["existing_session_label"],
            user_columns=["new_session_label"],
            suggest=suggest,
            output_dir=output_dir,
        )


def collect_original_subjs(
    fw: flywheel.Client,
    project: flywheel.Project,
    output_dir: Path = "/flywheel/v0/output",
    suggest: bool = True,
):
    """
    Search the project to find all the subjects with IDs.

    Returns:
        csv report of all the subjects with their IDs.
    """
    subj_rows = []
    for subj in fw.subjects.iter_find(f"parents.project={project.id}"):
        subj_rows.append([subj.id, subj.label])

    subj_df = pd.DataFrame(subj_rows, columns=["id", "existing_label"]).drop_duplicates(
        subset="id"
    )
    log.info("Building subject CSV...")
    if not subj_df.empty:
        data2csv(
            subj_df,
            project.label,
            prefix="subject_codes",
            column_rename=["id", "existing_subject_label"],
            user_columns=["new_subject_label"],
            suggest=suggest,
            output_dir=output_dir,
        )


def search_acq_details(acq: flywheel.Acquisition, sanitize: bool = True):
    """
    Search the metadata to return a sanitized label (no spaces or ill-behaving characters) and SeriesNumber.

    Returns:
        list of sanitized label and SeriesNumber to add as a new row
    """
    # Get the info to populate for the acquisition
    acq.reload()
    # Make sure that image(s) have been converted by dcm2niix (type = nifti)
    # and are not ignored (by _ignore-BIDS tag)
    try:
        imgs = [
            i
            for i in acq.files
            if (
                i.type in ["NIfTI", "nifti"]
                and "ignore" not in i.name
                and "Localizer" not in i["classification"]["Intent"]
            )
        ]
    except (KeyError, TypeError) as e:
        if type(e) == TypeError:
            log.warning(
                f"{acq.label} was missing classification. There will be problems with curation."
            )
        else:
            log.debug(
                "No Intent was provided for the classification. Potentially including a scout or localizer."
            )
        imgs = [
            i
            for i in acq.files
            if (i.type in ["NIfTI", "nifti"] and "ignore" not in i.name)
        ]

    acqs_details = []
    if imgs:

        a = [
            "Functional" in i["classification"]["Intent"]
            for i in imgs
            if i.classification and i.classification.get("Intent")
        ]
        if not all(a):
            imgs = [imgs[0]]

        names = [i.name for i in imgs]
        unique_names = set(names)
        if len(unique_names) < len(names):
            log.critical(
                f"More than one nifti available for acq={acq.label}, filename(s)={unique_names}. Cannot map."
            )
            return acqs_details
        for img in imgs:
            sn = None
            # Collect the fields that are applicable and leave the rest for memory management
            # Scrub the SeriesNumber from acq.label, as the SeriesNumber will be in a separate column now.
            label = sanitize_name_general(acq.label)
            label = re.sub(r"\s+", "", label)
            # Search for SeriesNumbers 0-99. IF there are more than 99 acquisitions, that is a crazy scanning session.
            # By original default the SeriesNumber prepended the SeriesDescription (ca. 04.2022 and prior)
            search_str = r"^\d{1,2}-"
            if re.search(search_str, label):
                # Extract the SeriesNumber from the label (in case no SeriesNumber is
                # available from the metadata) before removing it.
                sn = label.split("-")[0]
                label = re.sub(search_str, "", label)
            try:
                img.reload()
                seriesNum = int(img.info["SeriesNumber"])
            except KeyError:
                if sn:
                    seriesNum = sn
                else:
                    seriesNum = np.nan
            # Could potentially expose ['info']['AcquisitionTime'] for timestamp to ensure run ordering,
            # but then the row will always be unique.
            sess_id = img.parents["session"]
            if not sanitize:
                label = acq.label
            acqs_details.append([label, seriesNum, sess_id])
    return acqs_details
