# Deprecated Version
This version of bids-pre-curate is deprecated.  You are encouraged to only use the latest version in the new repository.  For instructions, please see the new [README](https://gitlab.com/flywheel-io/scientific-solutions/gears/bids-pre-curate)

## bids-pre-curation
Based on user-input, the bids pre-curation gear renames acquisitions, sessions, and subjects on a bulk scale.


# BIDS Pre-Curation (bids-pre-curate)

## Overview

[Usage](#usage)

[FAQ](#faq)

### Summary
Prepare a project for BIDS Curation. BIDS Pre-Curate offers a simple way to modify labels of project containers to be compatible with the [ReproIn Naming Convention](https://dbic-handbook.readthedocs.io/en/latest/mri/reproin.html) as recognized by the [reproin.json](https://gitlab.com/flywheel-io/public/bids-client/-/blob/master/flywheel_bids/templates/reproin.json) project curation template (or other possible templates). Running pre-curate on a given project (as a project-level analysis) will generate CSV files that will be populated with a unique list of container labels, as well as slots for the information needed for BIDS curation (classification, task, etc.). These CSV files can be downloaded and modified (outside of Flywheel) to provide missing or corrected information. The corrected CSV file is then uploaded to the project (as an attachment) and provided as input to a run of this same gear to do on-the-fly mappings and metadata updates.

### Cite
*license:* MIT


*url:* https://gitlab.com/flywheel-io/flywheel-apps/bids-pre-curate



### Classification
*Category:* Analysis

*Gear Level:*

- [x] Project

----

[[_TOC_]]

----


### Inputs
This section is autogenerated from the "inputs" section of the manifest

* *acquisition_table*
    - **Type**: File
    - **Optional**: True
    - **Description**: CSV file containing corrected information
    - **Notes**: Empty for stage 1; Required for stage 2
* *session_table*
    - **Type**: File
    - **Optional**: True
    - **Description**: CSV file containing corrected session information
  - **Notes**: Empty for stage 1; Required for stage 2
* *subject_table*
    - **Type**: File
    - **Optional**: True
    - **Description**: CSV file containing corrected subject information
    - **Notes**: Empty for stage 1; Required for stage 2

  
### Config

* allowed
    - **Type**: string
    - **Description**: Characters that will be allowed in names when suggesting new names.
    - **Default**: '_-'
* debug
    - **Type**: boolean
    - **Description**: Log debug messages.
    - **Default**: false
* dry_run
    - **Type**: boolean
    - **Description**: Whether or not to perform a dry run, logging what would be changed without actually changing it.
    - **Default**: false
* suggest
  - **Type**: boolean
  - **Description**: Whether or not to suggest new names by removing spaces and special characters from names.
  - **Default**: true


### Outputs
*{This section is autogenerated from a custom outputs section of the manifest}*

#### Files
When running "stage 1",
- acquisition_labels_{project}.csv
  - Details about existing acquisitions in the entire project
- session_labels_{project}.csv
  - Details about existing sessions in the entire project
- subject_codes_{project}.csv
  - Details about existing subjects across the entire project

When running "stage 2",
no outputs are produced.

#### Metadata
No changes

### Pre-requisites
Stage 1 (running the gear with no inputs) is required for Stage 2 (when the labels are actually changed in the database, based on user input).

#### Prerequisite Gear Runs
A list of gears, in the order they need to be run:

1. dcm2niix
    - Level: across entire project, likely already a gear rule
2. dcm_classifier
    - Level: across entire project, likely already a gear rule


## Usage

### Description
Stage 1:
`bids-pre-curate` searches across the entire project for unique subjects, sessions, and acquisitions. The gear records select, identifying details from all the results as CSV files corresponding to the various hierarchy levels.

Users must open the CSV files and modify the "new_{level}_label" columns to adhere to the ReproIn standard in order to take full advantage of BIDS curation on Flywheel.  Acquisitions can be ignored by putting "t", "y", or "x" in the Ignore column.
*** Users MUST check and enter/correct the "num_expected_runs" in the acquisition table. The gear will automatically exit before correcting any of the acquisitions, if this column is empty. ***  
![image](./docs/empty_col.png)
- Run counters can be automatically generated by the gear, if the new_acquisition_label column is given a ReproIn-compliant, but general (i.e., no "_run-XX" entities), name. Further details are outlined in the Use Cases below.

Stage 2:
User input from each of the hierarchy CSV files are handled separately. (Subject = `update_subject_labels`, Session = `update_labels`, and Acquisition = `update_acq_label_by_subj`)
- Rows without updates in the "new_{hierarchy-level}_label" column are removed as a first step. If there are no updates, then that hierarchy level method is skipped.
- Data is checked for BIDS compliance of new names, duplicate entries, and required additional run counters. Please reference the use cases below for more information about the generated run counters.
- Updates are made to the respective labels on the platform matching the corrective entries after checks are passed.


### Workflow

```mermaid
graph LR;
    A((Stage 1 <br>pre-curate)):::gear --> B;
    B[User downloads csvs <br> Corrects to ReproIn]:::input-->C;
    C{Uploads to<br>project}:::input-->E;
    E((Stage 2<br>pre-curate));
    E:::gear -.-> F((bids-curate)):::container;
    
    classDef container fill:#57d,color:#fff
    classDef input fill:#7a9,color:#fff
    classDef gear fill:#659,color:#fff

```

Description of workflow  
The gear initially collects information about the project and desired configuration, regardless of stage. 
### Stage 1
1. Gear run will:   
   1. Find all the subjects, sessions, and unique acquisitions that exist in the project.
   2. Each hierarchy level will have a specific csv returned for the user to check and correct.
   3. Specifically for acquisitions, filenames are searched for SeriesNumber entries that have been prepended to the acquisition labels. The SeriesNumber entries are stored in the acquisitions csv to allow general pattern matching, but also differentiate scan order (for run counters, in particular). 
   4. Output is produced as a CSV file on the analysis container. For this reason, best practice is to run the gear from the project level. (Much easier to find results.)
   
### Stage 2
   1. CSVs are ingested, cleaned, and transformed to dataframes.
   2. Non-empty dataframes are processed for unique and updating information.
   3. Each object at the specified hierarchy level within the project is checked to see if it matches with the unique and updating information.

### Use Cases
#### General use
`bids-precurate` is most useful for batch correction of retrospective data collected with naming conventions different from ReproIn at the scanner console. Single subjects can also be corrected with the same CSVs that have been attached to the project, so long as the original names correspond with the "existing_{level}_label" entries. (This method is slightly inefficient as the gear will inspect the entire project to be sure that ALL objects are up-to-date with the CSV entries.)

#### Run counters
- Direct specification in the CSV  
The [BIDS run entity](https://bids-specification.readthedocs.io/en/stable/99-appendices/09-entities.html#run) can be specified directly by the user in the corrected, acquisition CSV.
  
  - "EPI_nback_x" in the image below
- Generalized naming (see "wm" below) will have run counters appended automatically, if the run entity is not specified in the "new_acquisition_label" column AND the "num_expected_runs" match the number of observed acquisitions for a given subject.
- *Caveats*:
  - Specifying different numbers of num_expected_runs will result in a logged error message and no modifications for that label.
  - Specifying a different number of expected scans than are actually available for a subject will result in a logged error message and no modifications for that label.  
![image](./docs/run_counter_conditions.png)
### Logging
All logging from the gear is reported in the Gear Log. By default, these messages include anything from the INFO level through CRITICAL level.  
Messsages are intended to point out where user input differs from values expected by the gear, though caution has been taken to account for  as many variations as possible.  
CRITICAL messages most often terminate the gear.  
ERRORs should be carefully inspected to ensure the input was entered as intended.
WARNINGs alert about anomalies that likely will not affect curation.
INFO provides detail about the current step(s) in the gear.

## FAQ
[FAQ.md](FAQ.md)

## Contributing

[For more information about how to get started contributing to that gear,
checkout [CONTRIBUTING.md](CONTRIBUTING.md).]
